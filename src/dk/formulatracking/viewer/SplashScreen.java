package dk.formulatracking.viewer;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;

public class SplashScreen extends Activity {
	
	private Handler handler;
	SharedPreferences preferences;

	private Runnable mUpdateTimeTask = new Runnable() {
		public void run() {

			preferences = getSharedPreferences(GpsViewer.PREFERENCES,
					Activity.MODE_PRIVATE);
			new DownloadTask().execute();

			if (WebApi.getWebApi().isNewData_Unit()) {

				if (loadSetting("Username").equals("")
						|| loadSetting("Password").equals("")) {
					Intent intent = new Intent(SplashScreen.this,
							Settings.class);
					SplashScreen.this.startActivity(intent);
					SplashScreen.this.finish();
				}

				Intent mainIntent = new Intent(SplashScreen.this,
						GpsViewer.class);
				SplashScreen.this.startActivity(mainIntent);
				SplashScreen.this.finish();
			} else {

				if (WebApi.getWebApi().getErrorCode().equals("3")) {
					Intent intent = new Intent(SplashScreen.this,
							Settings.class);
					SplashScreen.this.startActivity(intent);
					SplashScreen.this.finish();
				} else {
					handler.postDelayed(mUpdateTimeTask, 10000);
				}
			}
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splashscreen);
		
		handler = new Handler();
	}

	@Override
	protected void onPause() {
		super.onPause();
		handler.removeCallbacks(mUpdateTimeTask);
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (loadSetting("Username").equals("")
				|| loadSetting("Password").equals("")) {
			Intent intent = new Intent(SplashScreen.this, Settings.class);
			SplashScreen.this.startActivity(intent);
			SplashScreen.this.finish();
		}

		 handler.postDelayed(mUpdateTimeTask, 1000);
	}

	private String loadSetting(String tag) {
		SharedPreferences preferences = getSharedPreferences(
				GpsViewer.PREFERENCES, Activity.MODE_PRIVATE);
		String content = preferences.getString(tag, "");
		return content;
	}

	private class DownloadTask extends AsyncTask<String, Void, Object> {
		protected Object doInBackground(String... args) {
			WebApi.getWebApi().Update_Unit(preferences);
			return args;

		}

		protected void onPostExecute(Object result) {
		}
	}

}