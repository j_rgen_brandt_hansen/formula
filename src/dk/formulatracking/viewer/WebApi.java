package dk.formulatracking.viewer;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import android.content.SharedPreferences;
import android.content.res.Resources.NotFoundException;
import android.os.AsyncTask;

public class WebApi {

	private String GetDataUrl_Unit = "http://www.#Domain/gps/api.aspx?user=#Username&pass=#Password&listall=true&ver=2";
	private String GetDataUrl_Poi = "http://www.#Domain/gps/api.aspx?user=#Username&pass=#Password&poi=catagories&ver=2";

	private boolean NewData_Unit = false;
	private boolean NewData_Poi = false;
	private boolean NewData_Poi_List = false;

	private boolean AutoUpdate = false;
	private String ErrorCode = "";
	private String ErrorText = "";

	public String getErrorText() {
		return ErrorText;
	}

	private String[] Unit_Unitsid;
	private String[] Unit_labels;
	private String[] Unit_addresses;
	private String[] Unit_timestamps;
	private String[] Unit_satellites;
	private String[] Unit_speeds;
	private String[] Unit_driver;
	private String[] Unit_batterylevels;
	private String[] Unit_latitudes;
	private String[] Unit_longtitudes;

	private String[] poi_numbers;
	private String[] poi_ids;
	private String[] poi_names;
	private String[] poi_counts;

	// Singleton
	private static WebApi webApi;
	DocumentBuilder db;
	URL methodUrl;

	public static synchronized WebApi getWebApi() {
		if (webApi == null) {
			webApi = new WebApi();
		}
		return webApi;
	}

	public void Update_Unit(SharedPreferences preferences) {
		NewData_Unit = false;

		// Get preferences
		String Username = preferences.getString("Username", "");
		String Password = preferences.getString("Password", "");
		String Domain = preferences.getString("Domain", "formulatracking.dk");
		// String Domain = preferences.getString("Domain", (String)
		// Resources.getSystem().getText(R.string.Domain));
		AutoUpdate = preferences.getBoolean("AutoUpdate", false);

		try {

			String UrlPath = GetDataUrl_Unit.replaceAll("#Domain", Domain)
					.replaceAll("#Username", Username)
					.replaceAll("#Password", Password);
			methodUrl = new URL(UrlPath);

			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			db = dbf.newDocumentBuilder();
			HttpGet httpGet = new HttpGet(UrlPath);
			HttpClient httpclient = new DefaultHttpClient();
			HttpResponse response = httpclient.execute(httpGet);
			Document doc = db.parse(new InputSource(response.getEntity()
					.getContent()));
			doc.getDocumentElement().normalize();

			NodeList nodeList = doc.getElementsByTagName("ErrorCode");
			Element nodeElement = (Element) nodeList.item(0);
			if (nodeElement == null) {
				// No Errorcode
				ErrorCode = "";
			} else {
				nodeList = nodeElement.getChildNodes();
				if (nodeList.getLength() > 0) {

					ErrorCode = ((Node) nodeList.item(0)).getNodeValue();
					if (ErrorCode.equals("3")) { // Forkert brugernavn og
													// kodeord

						ErrorText = "Forkert brugernavn og/eller kodeord";

					} else {
						ErrorText = "Ukendt fejl";

					}

				}
				return;
			}

			nodeList = doc.getElementsByTagName("Unit");

			Unit_Unitsid = new String[nodeList.getLength()];
			Unit_labels = new String[nodeList.getLength()];
			Unit_addresses = new String[nodeList.getLength()];
			Unit_timestamps = new String[nodeList.getLength()];
			Unit_satellites = new String[nodeList.getLength()];
			Unit_speeds = new String[nodeList.getLength()];
			Unit_driver = new String[nodeList.getLength()];
			Unit_batterylevels = new String[nodeList.getLength()];
			Unit_latitudes = new String[nodeList.getLength()];
			Unit_longtitudes = new String[nodeList.getLength()];

			for (int i = 0; i < nodeList.getLength(); i++) {
				Node node = nodeList.item(i);
				NodeList nList;
				// Element nodeElement;

				Element fstElmnt = (Element) node;

				nList = fstElmnt.getElementsByTagName("Desc");
				nodeElement = (Element) nList.item(0);
				nList = nodeElement.getChildNodes();
				if (nList.getLength() > 0)
					Unit_labels[i] = ((Node) nList.item(0)).getNodeValue();
				else
					Unit_labels[i] = "";

				nList = fstElmnt.getElementsByTagName("Country");
				nodeElement = (Element) nList.item(0);
				nList = nodeElement.getChildNodes();
				if (nList.getLength() > 0)
					Unit_addresses[i] = ((Node) nList.item(0)).getNodeValue();
				else
					Unit_addresses[i] = "-";

				nList = fstElmnt.getElementsByTagName("ZipCode");
				nodeElement = (Element) nList.item(0);
				nList = nodeElement.getChildNodes();
				if (nList.getLength() > 0)
					Unit_addresses[i] += "-"
							+ ((Node) nList.item(0)).getNodeValue();

				nList = fstElmnt.getElementsByTagName("City");
				nodeElement = (Element) nList.item(0);
				nList = nodeElement.getChildNodes();
				if (nList.getLength() > 0)
					Unit_addresses[i] += " "
							+ ((Node) nList.item(0)).getNodeValue();

				nList = fstElmnt.getElementsByTagName("Street");
				nodeElement = (Element) nList.item(0);
				nList = nodeElement.getChildNodes();
				if (nList.getLength() > 0) {
					String Street = ((Node) nList.item(0)).getNodeValue();
					if (Street.length() > 1)
						Unit_addresses[i] = ((Node) nList.item(0))
								.getNodeValue() + "\n" + Unit_addresses[i];
				}

				nList = fstElmnt.getElementsByTagName("Driver");
				nodeElement = (Element) nList.item(0);
				nList = nodeElement.getChildNodes();
				if (nList.getLength() > 0)
					Unit_driver[i] = ((Node) nList.item(0)).getNodeValue();
				else
					Unit_driver[i] = "";

				nList = fstElmnt.getElementsByTagName("Time");
				nodeElement = (Element) nList.item(0);
				nList = nodeElement.getChildNodes();
				if (nList.getLength() > 0)
					Unit_timestamps[i] = ((Node) nList.item(0)).getNodeValue();
				else
					Unit_timestamps[i] = "";
				if (Unit_timestamps[i] != "") {
					SimpleDateFormat format = new SimpleDateFormat(
							"dd-MM-yyyy HH:mm:ss");
					try {
						Calendar c = Calendar.getInstance();
						double z = (double) c.getTimeZone().getOffset(
								c.getTimeInMillis()) / 3600000.0;

						Date date = format.parse(Unit_timestamps[i]);
						c.setTime(date);
						c.add(c.HOUR, (int) z);
						date = c.getTime();
						format = new SimpleDateFormat("dd-MM-yyyy HH:mm");
						Unit_timestamps[i] = format.format(date);
					} catch (Exception e) {
					}
				}

				nList = fstElmnt.getElementsByTagName("Sat");
				nodeElement = (Element) nList.item(0);
				nList = nodeElement.getChildNodes();
				if (nList.getLength() > 0)
					Unit_satellites[i] = ((Node) nList.item(0)).getNodeValue();
				else
					Unit_satellites[i] = "?";

				nList = fstElmnt.getElementsByTagName("Speed");
				nodeElement = (Element) nList.item(0);
				nList = nodeElement.getChildNodes();
				if (nList.getLength() > 0)
					Unit_speeds[i] = ((Node) nList.item(0)).getNodeValue();
				else
					Unit_speeds[i] = "0";

				nList = fstElmnt.getElementsByTagName("ID");
				nodeElement = (Element) nList.item(0);
				nList = nodeElement.getChildNodes();
				if (nList.getLength() > 0) {
					Unit_Unitsid[i] = ((Node) nList.item(0)).getNodeValue();

				} else {
					Unit_Unitsid[i] = "0";

				}

				nList = fstElmnt.getElementsByTagName("Lat");
				nodeElement = (Element) nList.item(0);
				nList = nodeElement.getChildNodes();
				if (nList.getLength() > 0)
					Unit_latitudes[i] = ((Node) nList.item(0)).getNodeValue()
							.replaceAll(",", ".");
				else
					Unit_latitudes[i] = "0";

				nList = fstElmnt.getElementsByTagName("Lon");
				nodeElement = (Element) nList.item(0);
				nList = nodeElement.getChildNodes();
				if (nList.getLength() > 0)
					Unit_longtitudes[i] = ((Node) nList.item(0)).getNodeValue()
							.replaceAll(",", ".");
				else
					Unit_longtitudes[i] = "0";

				nList = fstElmnt.getElementsByTagName("BattPro");
				nodeElement = (Element) nList.item(0);
				nList = nodeElement.getChildNodes();
				if (nList.getLength() > 0)
					Unit_batterylevels[i] = ((Node) nList.item(0))
							.getNodeValue();
				else {
					nList = fstElmnt.getElementsByTagName("Battery");
					nodeElement = (Element) nList.item(0);
					nList = nodeElement.getChildNodes();
					if (nList.getLength() > 0) {
						String sBatteryLevel = ((Node) nList.item(0))
								.getNodeValue();
						Double iBatteryLevel = Double.parseDouble(sBatteryLevel
								.replace(',', '.'));
						if (iBatteryLevel >= 4.0)
							Unit_batterylevels[i] = "100";
						else if (iBatteryLevel >= 3.9)
							Unit_batterylevels[i] = "70";
						else if (iBatteryLevel >= 3.8)
							Unit_batterylevels[i] = "50";
						else if (iBatteryLevel >= 3.7)
							Unit_batterylevels[i] = "40";
						else if (iBatteryLevel >= 3.6)
							Unit_batterylevels[i] = "30";
						else if (iBatteryLevel >= 3.5)
							Unit_batterylevels[i] = "20";
						else
							Unit_batterylevels[i] = "10";

					} else
						Unit_batterylevels[i] = "";
				}

			}

		}

		catch (Exception e) {
			return;
		}
		NewData_Unit = true;
	}

	public void setErrorText(String errorText) {
		ErrorText = errorText;
	}

	public void Update_Poi(SharedPreferences preferences) {
		NewData_Poi = false;

		// Get preferences
		String Username = preferences.getString("Username", "");
		String Password = preferences.getString("Password", "");
		String Domain = preferences.getString("Domain", "");
		AutoUpdate = preferences.getBoolean("AutoUpdate", false);

		try {

			String UrlPath = GetDataUrl_Poi.replaceAll("#Domain", Domain)
					.replaceAll("#Username", Username)
					.replaceAll("#Password", Password);
			URL url = new URL(UrlPath);

			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			HttpGet httpGet = new HttpGet(UrlPath);
			HttpClient httpclient = new DefaultHttpClient();
			HttpResponse response = httpclient.execute(httpGet);
			Document doc = db.parse(new InputSource(response.getEntity()
					.getContent()));
			doc.getDocumentElement().normalize();

			NodeList nodeList = doc.getElementsByTagName("Errorcode");
			if (nodeList.getLength() > 0) {

				nodeList = doc.getElementsByTagName("Description");
				Element nodeElement = (Element) nodeList.item(0);
				nodeList = nodeElement.getChildNodes();
				if (nodeList.getLength() > 0)
					ErrorCode = ((Node) nodeList.item(0)).getNodeValue();
				else
					ErrorCode = "Unknown error.";
				return;
			} else {
				nodeList = doc.getElementsByTagName("Catagory");

				poi_numbers = new String[nodeList.getLength()];
				poi_ids = new String[nodeList.getLength()];
				poi_names = new String[nodeList.getLength()];
				poi_counts = new String[nodeList.getLength()];

				for (int i = 0; i < nodeList.getLength(); i++) {
					Node node = nodeList.item(i);
					NodeList nList;
					Element nodeElement;

					Element fstElmnt = (Element) node;

					nList = fstElmnt.getElementsByTagName("No");
					nodeElement = (Element) nList.item(0);
					nList = nodeElement.getChildNodes();
					if (nList.getLength() > 0) {
						poi_numbers[i] = ((Node) nList.item(0)).getNodeValue();
					} else {
						poi_numbers[i] = "";
					}

					nList = fstElmnt.getElementsByTagName("ID");
					nodeElement = (Element) nList.item(0);
					nList = nodeElement.getChildNodes();
					if (nList.getLength() > 0) {
						poi_ids[i] = ((Node) nList.item(0)).getNodeValue();
					} else {
						poi_ids[i] = "";
					}

					nList = fstElmnt.getElementsByTagName("Name");
					nodeElement = (Element) nList.item(0);
					nList = nodeElement.getChildNodes();
					if (nList.getLength() > 0) {
						poi_names[i] = ((Node) nList.item(0)).getNodeValue();
					} else {
						poi_names[i] = "";
					}

					nList = fstElmnt.getElementsByTagName("Count");
					nodeElement = (Element) nList.item(0);
					nList = nodeElement.getChildNodes();
					if (nList.getLength() > 0) {
						poi_counts[i] = "("
								+ ((Node) nList.item(0)).getNodeValue() + ")";
					} else {
						poi_counts[i] = "";
					}
				}

			}
		} catch (Exception e) {
			return;
		}
		NewData_Poi = true;
	}

	// ------------------------- get & set -------------------------------

	public boolean isNewData_Poi_List() {
		return NewData_Poi_List;
	}

	public String[] getUnit_Unitsid() {
		return Unit_Unitsid;
	}

	public String[] getUnit_labels() {
		return Unit_labels;
	}

	public String[] getUnit_addresses() {
		return Unit_addresses;
	}

	public String[] getUnit_timestamps() {
		return Unit_timestamps;
	}

	public String[] getUnit_satellites() {
		return Unit_satellites;
	}

	public String[] getUnit_speeds() {
		return Unit_speeds;
	}

	public String[] getUnit_driver() {
		return Unit_driver;
	}

	public String[] getUnit_batterylevels() {
		return Unit_batterylevels;
	}

	public String[] getUnit_latitudes() {
		return Unit_latitudes;
	}

	public String[] getUnit_longtitudes() {
		return Unit_longtitudes;
	}

	public String[] getPoi_numbers() {
		return poi_numbers;
	}

	public String[] getPoi_ids() {
		return poi_ids;
	}

	public boolean isNewData_Unit() {
		return NewData_Unit;
	}

	public boolean isNewData_Poi() {
		return NewData_Poi;
	}

	public boolean isAutoUpdate() {
		return AutoUpdate;
	}

	public void setAutoUpdate(boolean autoUpdate) {
		AutoUpdate = autoUpdate;
	}

	public String getErrorCode() {
		return ErrorCode;
	}

	public String[] getPoi_counts() {
		return poi_counts;
	}

	public String[] getPoi_names() {
		return poi_names;
	}

	public void setUnit_Unitsid(String[] unit_Unitsid) {
		Unit_Unitsid = unit_Unitsid;
	}
}