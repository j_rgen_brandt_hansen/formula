package dk.formulatracking.viewer;

import org.json.JSONArray;

import android.app.Activity;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class GpsViewer extends ListActivity {
	private JSONArray jsonArray;
	public static String PREFERENCES = "PREFERENCES";
	private Handler handler;
	protected ProgressDialog progressDialog;

	private Runnable runUpdateTask = new Runnable() {
		public void run() {

			new AsyncLoader().execute();
		}
	};

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		handler = new Handler();
		ListView listView = getListView();
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long id) {
				saveSetting("sCommand", "oneunit");
				saveSetting("SelectedUnitIndex", "" + position);
				saveSetting("SelectedUnitId",
						WebApi.getWebApi().getUnit_Unitsid()[position].toString());
				Intent i = new Intent(GpsViewer.this, HelloGoogleMaps.class);
				startActivity(i);
				
			}
		});
	}

	@Override
	public void onResume() {
		super.onResume();
		handler.post(runUpdateTask);
	}

	@Override
	public void onPause() {
		super.onPause();
		handler.removeCallbacks(runUpdateTask);
	}

	@Override
	public void onStop() {
		super.onStop();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}
	
	@Override
	public void onBackPressed() {
	}

	private void updateScreen() {
		if (WebApi.getWebApi().isNewData_Unit() == true) {
			setListAdapter();
		}
	}

	private void setListAdapter() {
		this.setListAdapter(new MyArrayAdapter(this, WebApi.getWebApi()
				.getUnit_Unitsid(), WebApi.getWebApi().getUnit_labels(), WebApi
				.getWebApi().getUnit_addresses(), WebApi.getWebApi()
				.getUnit_timestamps(), WebApi.getWebApi()
				.getUnit_batterylevels(), WebApi.getWebApi().getUnit_speeds(),
				WebApi.getWebApi().getUnit_satellites(), WebApi.getWebApi()
						.getUnit_latitudes(), WebApi.getWebApi()
						.getUnit_longtitudes(), WebApi.getWebApi()
						.getUnit_driver()));

		jsonArray = new JSONArray();
		for (int i = 0; i < WebApi.getWebApi().getUnit_Unitsid().length; i++) {
			jsonArray.put(WebApi.getWebApi().getUnit_Unitsid()[i]);
		}
		saveSetting("allUnits", jsonArray.toString());
	}

	
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);

		saveSetting("sCommand", "oneunit");
		saveSetting("SelectedUnitIndex", "" + position);
		saveSetting("SelectedUnitId",
				WebApi.getWebApi().getUnit_Unitsid()[position]);
		Intent i = new Intent(GpsViewer.this, HelloGoogleMaps.class);
		startActivity(i);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {

		case R.id.menu_update: // Update
			handler.post(runUpdateTask);
			return true;

		case R.id.menu_settings: // Ops�tning
			Intent intent = new Intent(GpsViewer.this, Settings.class);
			startActivity(intent);
			return true;

		case R.id.menu_hellogooglemaps: // Alle p� kort
			saveSetting("sCommand", "allunits");
			saveSetting("allUnits", jsonArray.toString());
			Intent map = new Intent(GpsViewer.this, HelloGoogleMaps.class);
			startActivity(map);
			return true;

		}
		return true;
	}

//	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
//		super.onCreateOptionsMenu(menu);
//
//		MenuItem item = menu.add(0, 1, 1, "Opdater");
//		item.setIcon(R.drawable.icon_update);
//
//		item = menu.add(0, 2, 2, "Ops�tning");
//		item.setIcon(R.drawable.icon_settings);
//
//		item = menu.add(0, 3, 3, "Alle p� kort");
//		item.setIcon(R.drawable.icon_hybridmap);
//
//		return true;
//	}
//	
//	public boolean onOptionsItemSelected(MenuItem item) {
//		if (item.getItemId() == 1) // Refresh
//		{
//			Intent intent = new Intent(GpsViewer.this, GpsViewer.class);
//			startActivity(intent);
//		} else if (item.getItemId() == 2) // Settings
//		{
//			Intent i = new Intent(GpsViewer.this, Settings.class);
//			startActivity(i);
//		} else if (item.getItemId() == 3) // View map
//		{
//			saveSetting("sCommand", "allunits");
//			saveSetting("allUnits", jsonArray.toString());
//			Intent i = new Intent(GpsViewer.this, HelloGoogleMaps.class);
//			startActivity(i);
//		}
//		return true;
//	}


	private void saveSetting(String tag, String content) {
		SharedPreferences preferences = getSharedPreferences(
				GpsViewer.PREFERENCES, Activity.MODE_PRIVATE);
		SharedPreferences.Editor editor = preferences.edit();
		editor.putString(tag, content);
		editor.commit();
	}

	private Boolean loadSettingBoolean(String tag) {
		SharedPreferences preferences = getSharedPreferences("PREFERENCES",
				Activity.MODE_PRIVATE);
		Boolean content = preferences.getBoolean(tag, false);
		return content;
	}

	private class AsyncLoader extends AsyncTask<Void, Void, Void> {

		protected void onPreExecute() {
			super.onPreExecute();
		}

		protected Void doInBackground(Void... arg0) {
			SharedPreferences preferences = getSharedPreferences("PREFERENCES",
					Activity.MODE_PRIVATE);

			WebApi.getWebApi().Update_Unit(preferences);
			// Start reload Timer
			boolean isNewDataJobs = WebApi.getWebApi().isNewData_Unit();
			if (isNewDataJobs == true) {
				runOnUiThread(new Runnable() {
					public void run() {
						updateScreen();
					}
				});

				if (loadSettingBoolean("AutoUpdate") == true) {
					handler.postDelayed(runUpdateTask, 30000);
				}
			} else {
				handler.postDelayed(runUpdateTask, 3000);
			}

			return null;
		}

		protected void onPostExecute(Void bool) {
			super.onPostExecute(bool);

			if (progressDialog != null) {
				progressDialog.hide();
			}
		}

		protected void onProgressUpdate(Void... values) {
			super.onProgressUpdate(values);
		}
	}

	private class PauseDialogLoader extends AsyncTask<Void, Integer, Void> {

		protected void onPreExecute() {
			super.onPreExecute();

			progressDialog = new ProgressDialog(GpsViewer.this,
					R.style.SkyhostProgressDialog);
			progressDialog.setTitle("Vent venligst...");
			progressDialog.setMessage("Data hentes...");
			progressDialog.setCancelable(false);
			progressDialog.setIndeterminate(true);
			progressDialog.show();

			// Set Runnable to remove splash screen just in case
			final Handler handler = new Handler();
			handler.postDelayed(new Runnable() {
				@Override
				public void run() {
					if (progressDialog != null) {
						progressDialog.dismiss();
					}
				}
			}, 5000);
		}

		protected Void doInBackground(Void... arg0) {
			return null;
		}

		protected void onPostExecute(Void... params) {
			super.onPostExecute(null);
			if (progressDialog != null) {
				progressDialog.hide();
			}
		}
	}
}