package dk.formulatracking.viewer;

import org.json.JSONArray;
import org.json.JSONException;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.GoogleMap.OnCameraChangeListener;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class HelloGoogleMaps extends FragmentActivity implements
		OnInfoWindowClickListener, InfoWindowAdapter {

	public static final String PREFERENCES = "PREFERENCES";
	static final int REQUEST_CODE_RECOVER_PLAY_SERVICES = 1001;
	static final int REQUEST_CODE_PICK_ACCOUNT = 1002;

	private LatLngBounds bounds;
	private Handler handler;
	private JSONArray jsonArrayAllUnits;
	private String sCommand;
	private GoogleMap googleMap;
	private GoogleMapOptions options;
	public Marker marker;
	private LatLng point;

	private Runnable runUpdateTask = new Runnable() {
		public void run() {
			SharedPreferences preferences = getSharedPreferences("PREFERENCES",
					Activity.MODE_PRIVATE);

			WebApi.getWebApi().Update_Unit(preferences);

			updateScreen();

			if (preferences.getBoolean("AutoUpdate", false) == true) {
				handler.postDelayed(runUpdateTask, 30000);
			}
		}
	};

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.map);

		googleMap = ((SupportMapFragment) getSupportFragmentManager()
				.findFragmentById(R.id.mapview)).getMap();
		
		googleMap.setOnCameraChangeListener(new OnCameraChangeListener() {

			@Override
			public void onCameraChange(CameraPosition arg0) {
				if(sCommand.equals("allunits")){
					 // Move camera.
					googleMap.animateCamera(CameraUpdateFactory
							.newLatLngBounds(bounds, 40));
				}else{
					
					CameraPosition cameraPosition = new CameraPosition.Builder()
				    .target(point)      // Sets the center of the map to Mountain View
				    .zoom(18)           // Sets the zoom
				    .build();           // Creates a CameraPosition from the builder
				googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
				}
				// Remove listener to prevent position reset on camera move.
		        googleMap.setOnCameraChangeListener(null);	
			}
		});

		try {
			if (!loadSetting("allUnits").equals("")) {
				jsonArrayAllUnits = new JSONArray(loadSetting("allUnits"));
			}
		} catch (JSONException e) {
			return;
		}
		handler = new Handler();
		 getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
	}
	
	@Override
	protected void onResume() {
		super.onResume();

		updateScreen();

		handler.postDelayed(runUpdateTask, 30000);
	}

	@Override
	protected void onPause() {
		super.onPause();
		handler.removeCallbacks(runUpdateTask);
	}

	private void updateScreen() {
		googleMap = ((SupportMapFragment) getSupportFragmentManager()
				.findFragmentById(R.id.mapview)).getMap();
		int height = 400;
		int width = 400;
		try {
			DisplayMetrics metrics = new DisplayMetrics();
			getWindowManager().getDefaultDisplay().getMetrics(metrics);

			height = (int) ((int) metrics.heightPixels);
			width = (int) ((int) metrics.widthPixels);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		googleMap.clear();
		sCommand = loadSetting("sCommand");
		LatLngBounds.Builder builder = new LatLngBounds.Builder();

		if (sCommand.equals("allunits")) {

			try {
				jsonArrayAllUnits = new JSONArray(loadSetting("allUnits"));

				for (int i = 0; i < jsonArrayAllUnits.length(); i++) {

					double lat = Double.parseDouble(WebApi.getWebApi()
							.getUnit_latitudes()[i]);
					double lng = Double.parseDouble(WebApi.getWebApi()
							.getUnit_longtitudes()[i]);

					if (lat != 0) {
						LatLng point = new LatLng(lat, lng);

						Marker markerId = googleMap
								.addMarker(new MarkerOptions()
										.position(point)

										.title(WebApi.getWebApi()
												.getUnit_labels()[i])
										.snippet(
												WebApi.getWebApi()
														.getUnit_addresses()[i])
										.icon(BitmapDescriptorFactory
												.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));

						builder.include(markerId.getPosition());
						googleMap
								.setOnMarkerClickListener(new OnMarkerClickListener() {

									public boolean onMarkerClick(Marker marker) {
										marker.showInfoWindow();
										return false;
									}
								});
					}
					bounds = builder.build();
					googleMap.getUiSettings().setCompassEnabled(true);
					googleMap.getUiSettings().setZoomControlsEnabled(true);
				}
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		}

		// One Unit position / Unit position
		else if (sCommand.equals("oneunit")) {
			int index = 0;
			String selectedUnitIndex = "";
			if (!(loadSetting("SelectedUnitIndex").equals(""))) {
				selectedUnitIndex = loadSetting("SelectedUnitIndex");
				index = Integer.parseInt(selectedUnitIndex);
			}

			double lat = Double.parseDouble(WebApi.getWebApi()
					.getUnit_latitudes()[index]);
			double lng = Double.parseDouble(WebApi.getWebApi()
					.getUnit_longtitudes()[index]);

			if (lat != 0) {
				point = new LatLng(lat, lng);

				Marker markerId = googleMap
						.addMarker(new MarkerOptions()
								.position(point)

								.title(WebApi.getWebApi().getUnit_labels()[index])
								.snippet(
										WebApi.getWebApi().getUnit_addresses()[index])
								.icon(BitmapDescriptorFactory
										.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));

				builder.include(markerId.getPosition());
				bounds = builder.build();
				
				googleMap.getUiSettings().setCompassEnabled(true);
				googleMap.getUiSettings().setZoomControlsEnabled(true);
			}
		}
	}
	
	protected boolean isRouteDisplayed() {
		return false;
	}

	public void setPowerSaveState(boolean enabled) {
		if (enabled == false)
			android.provider.Settings.System.putInt(getContentResolver(),
					android.provider.Settings.System.SCREEN_OFF_TIMEOUT, -1);
		else
			android.provider.Settings.System.putInt(getContentResolver(),
					android.provider.Settings.System.SCREEN_OFF_TIMEOUT, 60000);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.map, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {

		case R.id.menu_map_all_on_map: // Update
			saveSetting("sCommand", "allunits");
			Intent all = new Intent(HelloGoogleMaps.this, HelloGoogleMaps.class);
			startActivity(all);
			return true;

		case R.id.menu_map_normal_satellite: // Map
			if (googleMap.getMapType() == googleMap.MAP_TYPE_SATELLITE) {
				googleMap.setMapType(googleMap.MAP_TYPE_NORMAL);
			} else if (googleMap.getMapType() == googleMap.MAP_TYPE_NORMAL) {
				googleMap.setMapType(googleMap.MAP_TYPE_SATELLITE);
			}
			return true;

		case R.id.menu_map_main: // Settings
			Intent intent = new Intent(HelloGoogleMaps.this, GpsViewer.class);
			startActivity(intent);
			return true;

		}
		return true;
	}

	
	private void saveSetting(String tag, String content) {
		SharedPreferences preferences = getSharedPreferences(
				HelloGoogleMaps.PREFERENCES, Activity.MODE_PRIVATE);
		SharedPreferences.Editor editor = preferences.edit();
		editor.putString(tag, content);
		editor.commit();
	}

	private String loadSetting(String tag) {
		SharedPreferences preferences = getSharedPreferences(
				HelloGoogleMaps.PREFERENCES, Activity.MODE_PRIVATE);
		String content = preferences.getString(tag, "");
		return content;
	}

	private Boolean loadSettingBoolean(String tag) {
		SharedPreferences preferences = getSharedPreferences("PREFERENCES",
				Activity.MODE_PRIVATE);
		Boolean content = preferences.getBoolean(tag, false);
		return content;
	}

	@Override
	public void onInfoWindowClick(Marker marker) {
	}

	@Override
	public View getInfoContents(Marker marker) {
		marker.getId();
		return null;
	}

	@Override
	public View getInfoWindow(Marker arg0) {

		return null;
	}

}