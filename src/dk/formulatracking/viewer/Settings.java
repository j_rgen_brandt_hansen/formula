package dk.formulatracking.viewer;

import dk.formulatracking.viewer.R;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

public class Settings extends Activity {

	public void onCreate(Bundle icicle) {
		super.onCreate(icicle);
		setContentView(R.layout.settings);

		SharedPreferences preferences = getSharedPreferences(
				GpsViewer.PREFERENCES, Activity.MODE_PRIVATE);

		EditText NewUsername = (EditText) findViewById(R.id.Username);
		EditText NewPassword = (EditText) findViewById(R.id.Password);

		ToggleButton NewAutoUpdateSetting = (ToggleButton) findViewById(R.id.AutoRefresh);

		NewUsername.setText(preferences.getString("Username", ""));
		NewPassword.setText(preferences.getString("Password", ""));
		NewAutoUpdateSetting.setChecked(preferences.getBoolean("AutoUpdate",
				false));
		if (!WebApi.getWebApi().getErrorText().equals("")) {
			LinearLayout linearLayout = (LinearLayout) findViewById(R.id.linearLayout4);
			TextView textView_ErrorCode = new TextView(this);
			String error = WebApi.getWebApi().getErrorText();
			textView_ErrorCode.setText(WebApi.getWebApi().getErrorText());
			textView_ErrorCode.setTextSize(20);
			textView_ErrorCode.setPadding(20, 0, 20, 0);
			textView_ErrorCode.setTypeface(null, Typeface.BOLD);
			textView_ErrorCode.setTextColor(Color.BLACK);
			linearLayout.addView(textView_ErrorCode);
		}

		Button b = (Button) findViewById(R.id.buttonBack);
		b.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {

				EditText NewUsername = (EditText) findViewById(R.id.Username);
				EditText NewPassword = (EditText) findViewById(R.id.Password);
				ToggleButton NewAutoUpdateSetting = (ToggleButton) findViewById(R.id.AutoRefresh);

				String userName = TextUtils.htmlEncode(
						NewUsername.getText().toString()).replace(" ", "");
				String password = TextUtils.htmlEncode(
						NewPassword.getText().toString()).replace(" ", "");
				
				SharedPreferences preferences = getSharedPreferences(
						GpsViewer.PREFERENCES, Activity.MODE_PRIVATE);
				SharedPreferences.Editor editor = preferences.edit();
				editor.putString("Username", userName);
				editor.putString("Password", password);
				if (NewAutoUpdateSetting.isChecked() == true) {
					editor.putBoolean("AutoUpdate", true);
				} else {
					editor.putBoolean("AutoUpdate", false);
				}
				// Commit the edits!
				editor.commit();
				WebApi.getWebApi().setErrorText("");
				setResult(RESULT_OK);
				Intent intent = new Intent(Settings.this, SplashScreen.class);
				Settings.this.startActivity(intent);
				finish();
			}
		});
	}

	@Override
	public void onBackPressed() {
		Intent intent = new Intent(Settings.this, SplashScreen.class);
		Settings.this.startActivity(intent);
	}
}
