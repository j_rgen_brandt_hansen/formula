package dk.formulatracking.viewer;

import dk.formulatracking.viewer.R;

import org.json.JSONArray;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class MyArrayAdapter extends ArrayAdapter<String> {
	public static final String PREFERENCES = "PREFERENCES";
	private final Activity context;
	private final String[] unitsid;
	private final String[] names;
	private final String[] addresses;
	private final String[] timestamps;
	private final String[] batterylevel;
	private final String[] speeds;
	private final String[] satellites;
	private final String[] latitudes;
	private final String[] longtitudes;
	private final String[] driver;
	
	private JSONArray jsonArray;

	public MyArrayAdapter(Activity context, String[] unitsid, String[] names, String[] addresses, String[] timestamps, String[] batterylevel, String[] speeds, String[] satellites, String[] latitudes, String[] longtitudes, String[] driver) {
		super(context, R.layout.rowlayout, names);
		this.context = context;
		this.unitsid = unitsid;
		this.names = names;
		this.addresses = addresses;
		this.timestamps = timestamps;
		this.batterylevel = batterylevel;
		this.speeds = speeds;
		this.satellites = satellites;
		this.latitudes = latitudes;
		this.longtitudes = longtitudes;
		this.driver = driver;
	}

	// static to save the reference to the outer class and to avoid access to
	// any members of the containing class
	static class ViewHolder {
		public ImageView battery;
		public ImageView speedIcon;
		public TextView speed;
		public TextView unitid;
		public TextView label;
		public TextView address;
		public TextView timestamp;
		public TextView batterylevel;
		public TextView satellites;
		public TextView lat;
		public TextView lon;
		public TextView driver;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		// ViewHolder will buffer the access to the individual fields of the row
		// layout
		ViewHolder holder;
		// Recycle existing view if passed as parameter
		// This will save memory and time on Android
		// This only works if the base layout for all classes are the same
		View rowView = convertView;
		if (rowView == null) {
			LayoutInflater inflater = context.getLayoutInflater();
			rowView = inflater.inflate(R.layout.rowlayout, null, true);
			holder = new ViewHolder();
			holder.label = (TextView) rowView.findViewById(R.id.label);
			holder.address = (TextView) rowView.findViewById(R.id.address);
			holder.timestamp = (TextView) rowView.findViewById(R.id.timestamp);
			holder.satellites = (TextView) rowView.findViewById(R.id.satcount);
			holder.battery = (ImageView) rowView.findViewById(R.id.icon_batt);
			holder.speed = (TextView) rowView.findViewById(R.id.speedvalue);
			holder.speedIcon = (ImageView) rowView.findViewById(R.id.icon_speed);
			holder.lat = (TextView) rowView.findViewById(R.id.lat);
			holder.lon = (TextView) rowView.findViewById(R.id.lon);
			holder.unitid = (TextView) rowView.findViewById(R.id.unitid);
			holder.driver = (TextView) rowView.findViewById(R.id.TextViewDriverName);
			rowView.setTag(holder);
		} else {
			holder = (ViewHolder) rowView.getTag();
		}

		holder.unitid.setText(unitsid[position]);
		holder.label.setText(names[position]);
		holder.address.setText(addresses[position]);
		holder.timestamp.setText(timestamps[position]);
		holder.satellites.setText(" " + satellites[position]);
		holder.lat.setText(latitudes[position]);
		holder.lon.setText(longtitudes[position]);
		holder.driver.setText(driver[position]);
		if(satellites[position] == "0" || satellites[position] == "3")
		{
			holder.satellites.setTextColor(R.color.red); 			
		}
		if(speeds[position].equals("0"))
		{
			holder.speed.setText("");
			holder.speedIcon.setImageResource(R.drawable.icon_parked);
		}
		else
		{
			holder.speed.setText(speeds[position]);
			holder.speedIcon.setImageResource(R.drawable.icon_speed);
		}
		String Batt = "";
		if(batterylevel[position] != null)
			Batt = batterylevel[position].toString();
		if(batterylevel[position] == "0")
		{
			holder.battery.setImageResource(R.drawable.batt0);
		}
		else if(batterylevel[position] == "10")
		{
			holder.battery.setImageResource(R.drawable.batt10);
		}
		else if(batterylevel[position] == "20")
		{
			holder.battery.setImageResource(R.drawable.batt20);
		}
		else if(batterylevel[position] == "30")
		{
			holder.battery.setImageResource(R.drawable.batt30);
		}
		else if(batterylevel[position] == "40")
		{
			holder.battery.setImageResource(R.drawable.batt40);
		}
		else if(batterylevel[position] == "50")
		{
			holder.battery.setImageResource(R.drawable.batt50);
		}
		else if(batterylevel[position] == "60")
		{
			holder.battery.setImageResource(R.drawable.batt60);
		}
		else if(batterylevel[position] == "70")
		{
			holder.battery.setImageResource(R.drawable.batt70);
		}
		else if(batterylevel[position] == "80")
		{
			holder.battery.setImageResource(R.drawable.batt80);
		}
		else if(batterylevel[position] == "90")
		{
			holder.battery.setImageResource(R.drawable.batt90);
		}
		else if(Batt.contains("100"))
		{
			holder.battery.setImageResource(R.drawable.batt100);
		}
		else
		{
			holder.battery.setImageResource(R.drawable.batt_none);
		}
		return rowView;
	}
}


